 # IOT Basics

 ## Industrial Revolution  

 1. **Industry 1.0** - Mechanization, Steam power , weaving Looms.
 2. **Industry 2.0** - Mass production, Assembly line, electrical energy.
 3. **Industry 3.0** - Automation, Computers & Electronics to handle factory work.
 4. **Industry 4.0** - Cyber physical systems, Internet of things(IOT),Network.


![1](extras/fourth-industrial-revolution.jpg)

 ## Industry 3.0

### Industry 3.0  

Industry 3.0 was a huge leap ahead wherein the advent of computer and automation ruled the industrial scene. It was during this period of transformation era where more and more robots were used in to the processes to perform the tasks which were performed by humans. E.g. Use of Programmable Logic Controllers(PLCs), Robots etc.  

  ### Industry 3.0 Hierarchy & Architecture

![2](extras/Interaction-between-hierarchies-Levels-in-Industry-30-and-Industry-40-source.png)  

1. **Field Devices** - They are at the bottom of the Hierarchy. These are the sensors or actuators present in the factory, they measuring things or actuate machines (doing some actions).
        * sensors
        * acutators
        * Motors 

2. **Control Devies** - These are the PCs , PLCs etc. They tell the field devices what to do and control them.
        * Microcontrollers
        * Computer Numeric Controls

3. **Station** - A set of Field devices & Control Devices together is called a station.
4. **Work Centres** - A set of stations together is called a work centre (Factories). They use softwares like **Scada & Historian**.
5. **Enterprise** - A set of Work Centres together becomes an Enterprise. They use softwares like **ERP & MES**.  

  ###The softwares used like Scada, Erp etc are used to store data, arrange data given to them by the field devices through the control devices.



  ### Industry 3.0 Protocols
 ![click for image](extras/5.jfif)
Protocols are the guidlines used to for sending and receiving data.

Some of the protocols used by Industry 3.0 are
1. Modbus 
2. Profi-Network
3. CANopen
4. EtherCAT

  ### Industry 4.0

It is industry 3.0 connected to the internet.

 Now we can remotely access the data recieved because of the industries connected to the internet.


  ### Industry 4.0 Architecture

 
 

 

 Now the data recieved is sent to the cloud.

 Some of the protocols used by the industry 4.0 are

  * **MQTT**
  * **Web Sockets**
  * **https**
  * **REST API**
   
  _All these protocols are optimized to send data to the cloud for data analysis_

![click for image](extras/5.jfif)


 ## Industry 3.0 to Industry 4.0

  #### Problems Faced with Industry 4.0 upgradation

  * **Cost.**
  * **Downtime.**
  * **Reliablity.**

  #### Parameter need to overcome from  Conversion From 3.0 to 4.0

  * Requirement of new skill staff
  * New hardware expense
  * Time requied for install 
  * Lack of Documentation
  * Properitary PLC prtocol 

### HOW TO CONVERT FROM 3.0 to 4.0 
 
 We can use a library called Shunya Interfaces. It can used on an embedded system like raspberry pi and use it as per our requirements.

![5](/extras/7.png) 

 Step 1: Identifying the most popular Industry 3.0 devices.

 Step 2: Study Protocols that these devices communicate.

 Step 3: Get data From the industry 3.0 devices.

 Step 4: Send the data to the cloud for industry 4.0


  After the data is sent to the internet/cloud, we can

  1. Store data in **Time Shared Data database(TSDB)** using tools like

    * Prometheus
    * InfluxDB
    * Things Board
    * Grafana(Analytics tool for analysis)

  2. Make a **Dashboard** of the datas recieved using tools like

  * Grafana 
  * Thingsboard

  3. **Analyse** Data using platforms like

  * AWS IoT
  * Google IoT
  * Azure IoT
  * Thingsboard  

  ![link](extras/9.jfif)

  4. Gets **Alerts** using platforms like 

  * Zaiper
  * Twilio 
  * Integromat
  * IFTTT

  This how the data inside the cloud looks like 

   ![8](/extras/8.png)





